require('dotenv').config()

export default {
  mode: 'universal',
  /*
   ** Headers of the page
   */
  head: {
    title: process.env.npm_package_name || '',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      {
        hid: 'description',
        name: 'description',
        content: process.env.npm_package_description || ''
      }
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }]
  },
  /*
   ** Customize the progress-bar color
   */
  loading: { color: '#fff' },
  /*
   ** Global CSS
   */
  css: [
    { src: '~/assets/style/main.scss', lang: 'scss' },
    { src: '~/assets/style/custom.css', lang: 'css' }
  ],
  /*
   ** Router
   */
  router: {
    extendRoutes(routes, resolve) {
      routes.push(
        {
          name: 'about',
          path: '/about',
          component: resolve(__dirname, 'pages/public/about.vue')
        },
        {
          name: 'pricing',
          path: '/pricing',
          component: resolve(__dirname, 'pages/public/pricing.vue')
        },
        {
          name: 'faqs',
          path: '/faqs',
          component: resolve(__dirname, 'pages/public/faqs.vue')
        },
        {
          name: 'contact',
          path: '/contact',
          component: resolve(__dirname, 'pages/public/contact.vue')
        },
        {
          name: 'login',
          path: '/login',
          component: resolve(__dirname, 'pages/auth/login.vue')
        },
        {
          name: 'register',
          path: '/register',
          component: resolve(__dirname, 'pages/auth/register.vue')
        },
        {
          name: 'password',
          path: '/password',
          component: resolve(__dirname, 'pages/auth/fp.vue')
        },
        {
          name: 'reset_password',
          path: '/reset_password',
          component: resolve(__dirname, 'pages/auth/rp.vue')
        }
      )
    }
  },
  /*
   ** Plugins to load before mounting the App
   */
  plugins: ['@/plugins/fa', '~/plugins/axios.js'],
  /*
   ** Nuxt.js modules
   */
  modules: [
    // Doc: https://buefy.github.io/#/documentation
    'nuxt-buefy',
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    '@nuxtjs/auth',
    '@nuxtjs/pwa',
    '@nuxtjs/eslint-module',
    '@nuxtjs/dotenv'
  ],
  /*
   ** Axios module configuration
   ** See https://axios.nuxtjs.org/options
   */
  axios: {
    baseURL: process.env.API_URL
  },
  auth: {
    strategies: {
      facebook: {
        client_id: process.env.FB_CLIENT_ID,
        userinfo_endpoint:
          'https://graph.facebook.com/v2.12/me?fields=name,email',
        scope: ['public_profile', 'email']
      },
      google: {
        client_id: process.env.G_CLIENT_ID,
        response_type: 'token',
        access_type: 'online'
      }
    },
    plugins: ['~/plugins/oauth.js']
  },
  /*
   ** Build configuration
   */
  build: {
    postcss: {
      preset: {
        features: {
          customProperties: false
        }
      }
    },
    /*
     ** You can extend webpack config here
     */
    extend(config, ctx) {
      // Run ESLint on save
      if (ctx.isDev && ctx.isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }
  }
}
