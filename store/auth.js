import Cookie from 'js-cookie'
import jwtDecode from 'jwt-decode'

export const state = () => ({
  token: null,
  currentUser: null
})

export const mutations = {
  setToken(s, p) {
    s.token = p
  },
  setCurrentUser(s, p) {
    s.currentUser = p
  },
  resetToken(s) {
    s.token = null
  },
  resetCurrentUser(s) {
    s.currentUser = null
  }
}

export const actions = {
  initAuth(vc, req) {
    let token
    let expiry

    if (req) {
      if (!req.headers.cookie) {
        return
      }

      const tokenCookie = req.headers.cookie
        .split(';')
        .find(c => c.trim().startsWith('token='))
      const expiryCookie = req.headers.cookie
        .split(';')
        .find(c => c.trim().startsWith('expiry='))

      if (!tokenCookie) {
        return
      }

      token = tokenCookie.split('=')[1]
      expiry = expiryCookie.split('=')[1]
    } else {
      token = localStorage.getItem('token')
      expiry = localStorage.getItem('expiry')
    }

    if (new Date().getTime() > Number.parseInt(expiry) || !token) {
      // vc.dispatch('auth/logout')
      return
    }

    vc.commit('setToken', token)
  },
  async login({ commit, dispatch }, params) {
    try {
      const { data } = await this.$axios.post('/api/login', params)

      commit('setToken', data.token)

      const expiry = jwtDecode(data.token).exp

      localStorage.setItem('token', data.token)
      localStorage.setItem(
        'expiry',
        new Date().getTime() + Number.parseInt(expiry) * 1000
      )
      Cookie.set('token', data.token)
      Cookie.set(
        'expiry',
        new Date().getTime() + Number.parseInt(expiry) * 1000
      )

      dispatch('setCurrentUser')

      return { error: false, params }
    } catch (err) {
      return { error: true }
    }
  },
  async loginSocial({ commit, dispatch }, params) {
    try {
      const { data } = await this.$axios.post('/oauth/token', {
        grant_type: 'social',
        client_id: 1,
        client_secret: 'IkCSBEsCaxXuESqPXixKfthXp8G3kKSVJJw8DHVf',
        provider: params.strategy,
        access_token: params.token
      })

      const token = data.access_token
      const expiry = data.expires_in

      commit('setToken', token)

      localStorage.setItem('token', token)
      localStorage.setItem(
        'expiry',
        new Date().getTime() + Number.parseInt(expiry) * 1000
      )
      Cookie.set('token', token)
      Cookie.set(
        'expiry',
        new Date().getTime() + Number.parseInt(expiry) * 1000
      )

      dispatch('setCurrentUser')

      this.app.router.push('/account')

      return { error: false }
    } catch (err) {
      return { error: true }
    }
  },
  async register({ commit, dispatch }, params) {
    try {
      const { data } = await this.$axios.post('/api/register', params)

      commit('setToken', data.token)

      const expiry = jwtDecode(data.token).exp

      localStorage.setItem('token', data.token)
      localStorage.setItem(
        'expiry',
        new Date().getTime() + Number.parseInt(expiry) * 1000
      )
      Cookie.set('token', data.token)
      Cookie.set(
        'expiry',
        new Date().getTime() + Number.parseInt(expiry) * 1000
      )

      dispatch('setCurrentUser')

      return { error: false }
    } catch (err) {
      return { error: true, messages: err.response.data.message.join(' ') }
    }
  },
  async update({ commit, dispatch }, params) {
    try {
      debugger // eslint-disable-line
      await this.$axios.put(`/api/users/${params.id}`, params)

      dispatch('setCurrentUser')

      return { error: false }
    } catch (err) {
      return { error: true }
    }
  },
  async requestResetPass({ commit, dispatch }, params) {
    try {
      await this.$axios.post('/api/forgot/password', params)
      return { error: false }
    } catch (err) {
      return { error: true }
    }
  },
  async resetPass({ commit, dispatch }, params) {
    try {
      const { data } = await this.$axios.post('/api/login/forgot', params)

      commit('setToken', data.data)

      const expiry = jwtDecode(data.data).exp

      localStorage.setItem('token', data.data)
      localStorage.setItem(
        'expiry',
        new Date().getTime() + Number.parseInt(expiry) * 1000
      )
      Cookie.set('token', data.data)
      Cookie.set(
        'expiry',
        new Date().getTime() + Number.parseInt(expiry) * 1000
      )

      dispatch('setCurrentUser')

      return { error: false }
    } catch (err) {
      return { error: true }
    }
  },
  async setCurrentUser({ commit }) {
    await this.$axios
      .get('api/user')
      .then(res => {
        commit('setCurrentUser', res.data.data[0])
      })
      // eslint-disable-next-line no-console
      .catch(err => console.log(err))
  },
  async logout({ commit }) {
    const { data } = await this.$axios.post('/api/logout')

    if (data.success) {
      commit('resetToken')
      commit('resetCurrentUser')
      Cookie.remove('token')
      Cookie.remove('expiry')
      if (process.client) {
        localStorage.removeItem('token')
        localStorage.removeItem('expiry')
      }
    }
  }
}

export const getters = {
  token: s => {
    return s.token
  },
  currentUser: s => {
    return s.currentUser
  }
}
