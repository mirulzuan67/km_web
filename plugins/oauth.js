export default function({ app, store }) {
  if (app.$auth.loggedIn) {
    const auth = app.$auth
    const strategy = auth.strategy.name

    if (strategy === 'facebook' || strategy === 'google') {
      const token = auth.getToken(strategy).substr(7)
      store
        .dispatch('auth/loginSocial', {
          strategy: strategy,
          token: token
        })
        .then(res => {
          if (res.error) {
            // eslint-disable-next-line no-console
            console.log('Invalid login credentials.')
          }
        })
    }
  }
}
